-- Implementation and verification of the Davis Putnam algorithm
module _ where

open import Agda.Builtin.Nat using (Nat; zero; suc)
open import Data.Bool using (Bool; true; false; not; T)
open import Data.Bool.Properties using (T?)
open import Data.List using (List; _∷_; []; filter; _++_; map)
open import Data.List.Relation.Unary.All using (All; all; tail; head; _∷_; [])
open import Data.List.Relation.Unary.Any using (Any; here; there) renaming (map to Any-map)
open import Data.Empty using (⊥; ⊥-elim)
open import Data.Unit using (⊤; tt)
open import Data.Nat.Properties renaming (_≟_ to _ℕ≟_)
open import Data.Product using (Σ; _,_; _×_; map₁; map₂; proj₁; proj₂)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Function.Equivalence using (_⇔_; equivalence; Equivalence)
open import Function.Equality using (_⟨$⟩_)
open import Function using (_∘_)
open import Relation.Binary using (REL; Rel; IsDecEquivalence)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong; setoid; isDecEquivalence)
open import Relation.Binary.Definitions renaming (Decidable to Decidable₂)
open import Relation.Nullary using (yes; no; Dec; ¬_)
open import Relation.Unary using (Decidable; ∁; Pred)
open import Relation.Unary.Properties using (∁?)
open import Relation.Binary.Bundles using (DecSetoid)

Var : Set
Var = Nat

data Literal : Set where
  pos : Var → Literal
  neg : Var → Literal

decSetoid-Literal : DecSetoid _ _
decSetoid-Literal = record
  { Carrier = Literal
  ; _≈_ = _≡_
  ; isDecEquivalence = isDecEquivalence dec }
  -- TODO can this be made shorter?
  where dec : Decidable₂ _≡_
        dec (pos x) (pos y) = aux (x ℕ≟ y)
          where
          pos-injec : ∀ {a b} → pos a ≡ pos b → a ≡ b
          pos-injec {zero} {zero} refl = refl
          pos-injec {zero} {suc b} ()
          pos-injec {suc a} {zero} ()
          pos-injec {suc a} {suc .a} refl = refl
          aux : ∀ {x y} → Dec (x ≡ y) → Dec (pos x ≡ pos y)
          aux (yes p) = yes (cong pos p)
          aux (no ¬p) = no λ x₂ → ¬p (pos-injec x₂)
        dec (pos x) (neg y) = no λ ()
        dec (neg x) (pos y) = no λ ()
        dec (neg x) (neg y) = aux (x ℕ≟ y)
          where
          neg-injec : ∀ {a b} → neg a ≡ neg b → a ≡ b
          neg-injec {zero} {zero} refl = refl
          neg-injec {zero} {suc b} ()
          neg-injec {suc a} {zero} ()
          neg-injec {suc a} {suc .a} refl = refl
          aux : ∀ {x y} → Dec (x ≡ y) → Dec (neg x ≡ neg y)
          aux (yes p) = yes (cong neg p)
          aux (no ¬p) = no λ x₂ → ¬p (neg-injec x₂)

_≟_ : Decidable₂ (_≡_)
_≟_ = IsDecEquivalence._≟_ (DecSetoid.isDecEquivalence decSetoid-Literal)

open import Data.List.Membership.Setoid (setoid Literal) using (_∈_; _∉_)
open import Data.List.Membership.DecPropositional {A = Literal} (_≟_) using (_∈?_)

∼_ : Literal → Literal
∼ pos x = neg x
∼ neg x = pos x

∼∼ : ∀ {l} → ∼ ∼ l ≡ l
∼∼ {pos x} = refl
∼∼ {neg x} = refl

¬∼ : ∀ {l} → ¬ (l ≡ ∼ l)
¬∼ {pos x₁} ()
¬∼ {neg x₁} ()

Clause : Set
Clause = List Literal

CNF : Set
CNF = List Clause

Interpretation : Set
Interpretation = Var → Bool

infix 4 _⊨l_
_⊨l_ : REL Interpretation Literal _
I ⊨l pos x = T (I x)
I ⊨l neg x = T (not (I x))

_⊨∨_ : REL Interpretation Clause _
I ⊨∨ c = Any (I ⊨l_) c

_⊨_ : REL Interpretation CNF _
I ⊨ cnf = All (I ⊨∨_) cnf

Sat : Pred CNF _
Sat cnf = Σ Interpretation (_⊨ cnf)

_≡ₛₐₜ_ : Rel CNF _
cnf1 ≡ₛₐₜ cnf2 = Sat cnf1 ⇔ Sat cnf2

Taut : Pred Clause _
Taut c = Σ Literal (λ v → v ∈ c × ∼ v ∈ c)

Taut? : Decidable Taut
Taut? ([]) = no λ { () }
Taut? (x ∷ lits) with ∼ x ∈? lits
... | yes y = yes (x , here refl , there y)
... | no n with Taut? lits
... | yes (l , a , b) = yes (l , there a , there b)
... | no n' = no λ { (l , here px , here refl) → ¬∼ px
      ; (l , there a , here refl) → n (Any-map (λ { refl → ∼∼} ) a)
      ; (l , here refl , there b) → n b
      ; (l , there a , there b) → n' (l , (a , b)) }

∈-Any : ∀ {l cl} {I : Interpretation} → I ⊨l l → l ∈ cl → I ⊨∨ cl
∈-Any x (here refl) = here x
∈-Any x (there y) = there (∈-Any x y)

¬T⇔T∘not : ∀ {x} → (¬ (T x)) ⇔ T (not x)
¬T⇔T∘not = equivalence ⇒ ⇐
  where
  ⇒ : ∀ {x} → (¬ (T x)) → T (not x)
  ⇒ {false} f = tt
  ⇒ {true} f = f tt
  ⇐ : ∀ {x} → T (not x) → (¬ (T x))
  ⇐ {false} f z = z

¬→not : ∀ {x} → ¬ (T x) → T (not x)
¬→not f = Equivalence.to ¬T⇔T∘not ⟨$⟩ f

not→¬ : ∀ {x} → T (not x) → ¬ (T x)
not→¬ f = Equivalence.from ¬T⇔T∘not ⟨$⟩ f

l∨∼l : ∀ I l → I ⊨l l ⊎ I ⊨l ∼ l
l∨∼l I (pos x) with T? (I x)
... | yes y = inj₁ y
... | no n = inj₂ (¬→not n)
l∨∼l I (neg x) with T? (I x)
... | yes y = inj₂ y
... | no n = inj₁ (¬→not n)

⊭l : ∀ I l → I ⊨l l → ¬ (I ⊨l ∼ l)
⊭l I (pos x₁) x y = not→¬ y x
⊭l I (neg x₁) x y = not→¬ x y

taut-⊨∨ : ∀ {c} → (I : Interpretation) → Taut c → I ⊨∨ c
taut-⊨∨ {c} I (l , fst , snd) with l∨∼l I l
... | inj₁ x = ∈-Any x fst
... | inj₂ y = ∈-Any y snd

-- Delete all tautologies
taut-rule : CNF → CNF
taut-rule = filter (∁? Taut?)

taut-rule-valid : ∀ {cnf : CNF} → cnf ≡ₛₐₜ taut-rule cnf
taut-rule-valid = equivalence ⇒ ⇐
  where
  ⇒ : {a : CNF} → Sat a → Sat (taut-rule a)
  ⇒ (I , x) = I , (⇒-aux I x)
    where
    ⇒-aux : {a : CNF} → (I : Interpretation) → I ⊨ a → I ⊨ taut-rule a
    ⇒-aux {[]} I p = p
    ⇒-aux {c ∷ cs} I (p) with Taut? c | ⇒-aux I ((tail p))
    ... | no _  | (r) = (head p ∷ r)
    ... | yes _ | r = r
  ⇐ : {a : CNF} → Sat (taut-rule a) → Sat a
  ⇐ (I , x) = I , (⇐-aux I x)
    where
    ⇐-aux : {a : CNF} → (I : Interpretation) → I ⊨ taut-rule a → I ⊨ a
    ⇐-aux {[]} I p = p
    ⇐-aux {c ∷ cs} I p with Taut? c
    ... | no n  = head p ∷ ⇐-aux I (tail p)
    ... | yes y = taut-⊨∨ I y ∷ ⇐-aux I p

[_]≡_ : REL Literal Clause _
[ l ]≡ [] = ⊥
[ l ]≡ (x ∷ []) with x ≟ l
... | yes _ = ⊤
... | no _ = ⊥
[ l ]≡ (x ∷ x₁ ∷ c) = ⊥

[_]∈_ : REL Literal CNF _
[ l ]∈ cnf = Any ([ l ]≡_) cnf

delete-lit : Literal → Clause → Clause
delete-lit l = filter (∁? (l ≟_))

-- Given a literal l where l is a singleton clause, delete all clauses
-- containing l and delete ∼l from all clauses.

OL : Literal → CNF → CNF
OL l = map (delete-lit (∼ l)) ∘ filter (∁? (l ∈?_))

one-literal-rule : ∀ {l cnf} →  [ l ]∈ cnf → CNF
one-literal-rule {l} {cnf} x = OL l cnf

¬⊨l : ∀ {I l} → I ⊨l l ⇔ (¬ (I ⊨l ∼ l))
¬⊨l {I} {l} = equivalence (⇒ {l} ) (⇐ {l})
  where
  ⇒ : ∀ {l} → I ⊨l l → (¬ (I ⊨l ∼ l))
  ⇒ {pos v} x y = not→¬ y x
  ⇒ {neg v} x y = not→¬ x y
  ⇐ : ∀ {l} → (¬ (I ⊨l ∼ l)) → I ⊨l l
  ⇐ {pos v} x = aux (¬→not x)
    where aux : ∀ {b} → T (not (not b)) → T b
          aux {true} x = tt
  ⇐ {neg v} x = ¬→not x

⟨_∪_⟩ : Interpretation → Literal → Interpretation
⟨ I ∪ pos x ⟩ y with x ℕ≟ y
... | yes _ = true
... | no _ = I y
⟨ I ∪ neg x ⟩ y with x ℕ≟ y
... | yes _ = false
... | no _ = I y

⟨I∪s⟩⊨s : ∀ I s → ⟨ I ∪ s ⟩ ⊨l s
⟨I∪s⟩⊨s I (pos x) with x ℕ≟ x
... | yes y = tt
... | no n = ⊥-elim (n refl)
⟨I∪s⟩⊨s I (neg x) with x ℕ≟ x
... | yes y = tt
... | no ¬p = ⊥-elim (¬p refl)

⟨I∪s⟩⊨t : ∀ I s l → I ⊨l l → ¬ (∼ s ≡ l) → ⟨ I ∪ s ⟩ ⊨l l
⟨I∪s⟩⊨t I (pos x) (pos y) Il ¬p with x ℕ≟ y
... | yes _ = tt
... | no _ = Il
⟨I∪s⟩⊨t I (pos x) (neg y) Il ¬p with x ℕ≟ y
... | yes p = ⊥-elim (¬p (cong neg p))
... | no _ = Il
⟨I∪s⟩⊨t I (neg x) (pos y) Il ¬p with x ℕ≟ y
... | yes p = ⊥-elim (¬p (cong pos p))
... | no _ = Il
⟨I∪s⟩⊨t I (neg x) (neg y) Il ¬p with x ℕ≟ y
... | yes p = tt
... | no _ = Il


one-literal-rule-valid : ∀ {l cnf} → (s : [ l ]∈ cnf) → cnf ≡ₛₐₜ one-literal-rule s
one-literal-rule-valid {l} {cnf1} s = equivalence ⇒ ⇐
  where
  ⇒ : Sat cnf1 → Sat (one-literal-rule s)
  ⇒ (I , snd) = I , ⇒-aux snd (l2 l cnf1 s snd)
    where
    l2 : ∀ l cnf → [ l ]∈ cnf → I ⊨ cnf → I ⊨l l
    l2 l ((l1 ∷ []) ∷ _) (here px) (here py ∷ y) with l1 ≟ l
    ... | yes refl = py
    {-# CATCHALL #-}
    l2 l (_ ∷ cs) (there x) (_ ∷ y) = l2 l cs x y
    ⇒-aux : ∀ {cnf s} → I ⊨ cnf → I ⊨l s → I ⊨ OL s cnf
    ⇒-aux {[]} x I⊨s = x
    ⇒-aux {c ∷ cs} {s} I⊨ccs I⊨s with s ∈? c
    ... | yes _ = ⇒-aux {cs} {s} (tail I⊨ccs) I⊨s
    ... | no _ = l1 (head I⊨ccs) I⊨s ∷ ⇒-aux {cs} {s} (tail I⊨ccs) I⊨s
      where
      l1 : ∀ {c s} → I ⊨∨ c → I ⊨l s → I ⊨∨ (delete-lit (∼ s) c)
      l1 {x ∷ cs} {s} (here px) I⊨s with (∼ s) ≟ x
      ... | yes refl = ⊥-elim (⊭l I s I⊨s px)
      ... | no _ = here px
      l1 {x ∷ cs} {s} (there I⊨cs) I⊨s with (∼ s) ≟ x
      ... | yes refl = l1 I⊨cs I⊨s
      ... | no n = there (l1 I⊨cs I⊨s)
  ⇐ : Sat (one-literal-rule s) → Sat cnf1
  ⇐ (I , snd) = ⟨ I ∪ l ⟩ , ⇐-aux snd
    where
    l2 : ∀ {c s} → I ⊨∨ delete-lit (∼ s) c → ⟨ I ∪ s ⟩ ⊨∨ c
    l2 {l ∷ ls} {s} I⊨dp with (∼ s) ≟ l | s ≟ l
    ... | yes y | c = there (l2 I⊨dp)
    ... | no n | yes refl = here (⟨I∪s⟩⊨s I l)
    ... | no n | no ¬p with I⊨dp
    ... | here px = here (⟨I∪s⟩⊨t I s l px n)
    ... | there x = there (l2 x)
    ⇐-aux : ∀ {cnf s} → I ⊨ OL s cnf → ⟨ I ∪ s ⟩ ⊨ cnf
    ⇐-aux {[]} I⊨OLcnf = []
    ⇐-aux {c ∷ cs} {s} I⊨OLccs with s ∈? c
    ... | yes y = ∈-Any (⟨I∪s⟩⊨s I s) y ∷ ⇐-aux I⊨OLccs
    ... | no n = l2 (head I⊨OLccs) ∷ ⇐-aux (tail I⊨OLccs)

-- pure literal rule: given a literal l such that its complement (∼ l) does not
-- appear in the cnf, remove every clause where it appears.

pure_∈_ : REL Literal CNF _
pure l ∈ cnf = Any (l ∈_) cnf × All (∼ l ∉_) cnf

-- removes every clause where l appears
PL : Literal → CNF → CNF
PL l = filter (∁? (l ∈?_))

pure-literal-rule : ∀ {l cnf} → pure l ∈ cnf → CNF
pure-literal-rule {l} {cnf} (_ , snd) = PL l cnf

pure-literal-rule-valid : ∀ {l cnf} → (p : pure l ∈ cnf) → cnf ≡ₛₐₜ pure-literal-rule p
pure-literal-rule-valid {l} {cnf} p = equivalence ⇒ ⇐
  where
  ⇒ : Sat cnf → Sat (pure-literal-rule p)
  ⇒ (I , snd) = I , ⇒-aux cnf I l snd
    where
    ⇒-aux : ∀ cnf I l → I ⊨ cnf → I ⊨ PL l cnf
    ⇒-aux [] I l x = x
    ⇒-aux (c ∷ cs) I l (p ∷ ps) with l ∈? c
    ... | yes y = ⇒-aux cs I l ps
    ... | no n = p ∷ ⇒-aux cs I l ps
  ⇐ : Sat (pure-literal-rule p) → Sat cnf
  ⇐ (I , snd) =  ⟨ I ∪ l ⟩ , ⇐-aux cnf I l (proj₂ p) snd
    where
    aux : ∀ I cl l → I ⊨∨ cl → ∼ l ∉ cl → ⟨ I ∪ l ⟩ ⊨∨ cl
    aux I (c ∷ cl) l I⊨∨cl ∼l∉cl with (∼ l) ≟ c
    ... | yes refl = ⊥-elim (∼l∉cl (here refl))
    ... | no c≠∼l with I⊨∨cl
    ... | here px = here (⟨I∪s⟩⊨t I l c px c≠∼l)
    ... | there h = there (aux I cl l h λ z → ∼l∉cl (there z))
    ⇐-aux : ∀ cnf I l → All (∼ l ∉_) cnf → I ⊨ PL l cnf → ⟨ I ∪ l ⟩ ⊨ cnf
    ⇐-aux [] I l x x₁ = []
    ⇐-aux (c ∷ cs) I l (px ∷ x) I⊨PLcnf with l ∈? c
    ... | yes y = ∈-Any (⟨I∪s⟩⊨s I l) y ∷ ⇐-aux cs I l x I⊨PLcnf
    ... | no ¬p with I⊨PLcnf
    ... | pc ∷ pcs = aux I c l pc px ∷ ⇐-aux cs I l x pcs
