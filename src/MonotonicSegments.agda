-- Find the statement in ref/ghc_sort.pdf

module _ where

open import Agda.Builtin.Nat using (Nat; zero; suc)
open import Agda.Builtin.Unit using (⊤; tt)
open import Data.List using (List; []; map; _∷_; _++_; foldr; concat; foldl; unsnoc; length; head; reverse)
open import Data.List.Properties using (++-identityʳ; ++-assoc)
open import Data.List.Relation.Unary.All using (All; []; _∷_)
open import Data.List.Reverse using (Reverse; reverseView)
open import Data.Maybe using (Maybe; just; nothing)
open import Data.Nat using (_<_; _≥_; _≤_; _>_)
open import Data.Nat.Properties using (_≥?_; <-cmp; ≤-refl; <⇒≤)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Relation.Binary using (Rel)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; sym; trans; _≢_)
open import Relation.Nullary using (yes; no; does)
open import Relation.Unary using (Pred; _∪_; _∩_)

monotonic~ : Rel Nat _ → Pred (List Nat) _
monotonic~ _ [] = ⊤
monotonic~ _ (x ∷ []) = ⊤
monotonic~ _~_ (x ∷ y ∷ l) = x ~ y × (monotonic~ _~_ (y ∷ l))

monotonic< : Pred (List Nat) _
monotonic< = monotonic~ _<_

monotonic≤ : Pred (List Nat) _
monotonic≤ = monotonic~ _≤_

monotonic≥ : Pred (List Nat) _
monotonic≥ = monotonic~ _≥_

monotonic> : Pred (List Nat) _
monotonic> = monotonic~ _>_

monotonic : Pred (List Nat) _
monotonic = monotonic< ∪ monotonic≥

monotonic-rev : Pred (List Nat) _
monotonic-rev = monotonic≤ ∪ monotonic>

monotonic2 : Pred (List Nat) _
monotonic2 [] = ⊤
monotonic2 (x ∷ []) = ⊤
monotonic2 l@(x ∷ y ∷ t) = monotonic l

monotonic-rev2 : Pred (List Nat) _
monotonic-rev2 [] = ⊤
monotonic-rev2 (x ∷ []) = ⊤
monotonic-rev2 l@(x ∷ y ∷ t) = monotonic-rev l

record MList : Set where
  constructor mlist
  field
    list : List Nat
    mono : monotonic2 list

record MList-rev : Set where
  constructor mlist
  field
    list : List Nat
    mono : monotonic-rev2 list

record Segments (l : List Nat) : Set where
  constructor Seg
  field
    segments : List MList
    idd : concat (map MList.list segments) ≡ l

cmp : (a b : Nat) → a < b ⊎ a ≥ b
cmp a b with <-cmp a b
... | Relation.Binary.tri< p _ _ = inj₁ p
... | Relation.Binary.tri≈ _ refl _ = inj₂ ≤-refl
... | Relation.Binary.tri> _ _ c = inj₂ (<⇒≤ c)

cmp-rev : (a b : Nat) → a ≤ b ⊎ a > b
cmp-rev a b with <-cmp a b
... | Relation.Binary.tri< a<b _ _ = inj₁ (<⇒≤ a<b)
... | Relation.Binary.tri≈ _ refl _ = inj₁ ≤-refl
... | Relation.Binary.tri> _ _ a>b = inj₂ a>b


mono-rev-aux : ∀ {l} → monotonic-rev l → monotonic (reverse l)
mono-rev-aux {[]} _ = inj₁ tt
mono-rev-aux {_ ∷ []} _ = inj₁ tt
mono-rev-aux {a ∷ b ∷ l} (inj₁ (a≤b , snd)) =
  let
    ih = mono-rev-aux (inj₁ snd)
  in
   {!!}
mono-rev-aux {a ∷ b ∷ l} (inj₂ y) = {!!}

MList-rev⇒MList : MList-rev → MList
MList-rev⇒MList (mlist list mono) =
 mlist (reverse list) {!!}

split : (l : List Nat) → List MList-rev
split l = reverse (proj₂ (foldl acc (mlist [] tt , []) l))
  where
  acc : (MList-rev × List MList-rev) → Nat → (MList-rev × List MList-rev)
  acc (mlist [] mono , ac) a = (mlist (a ∷ []) tt) , ac
  acc (mlist (a ∷ []) mono , ac) b with cmp-rev b a
  ... | inj₁ p = mlist (b ∷ a ∷ []) (inj₁ (p , tt)) , ac
  ... | inj₂ p = mlist (b ∷ a ∷ []) (inj₂ (p , tt)) , ac
  acc (ml@(mlist l@(a ∷ _ ∷ _) mono) , ac) b with mono , cmp-rev b a
  ... | inj₁ incr , inj₁ b≤a = mlist (b ∷ l) (inj₁ (b≤a , incr)) , ac
  ... | inj₁ incr , inj₂ b>a = mlist (b ∷ []) tt , ml ∷ ac
  ... | inj₂ decr , inj₁ b≤a = mlist (b ∷ []) tt , ml ∷ ac
  ... | inj₂ decr , inj₂ b>a = mlist (b ∷ l) (inj₂ (b>a , decr)) , ac

test = split (1 ∷ 4 ∷ 7 ∷ 3 ∷ 3 ∷ 5 ∷ 9 ∷ 2 ∷ [])

last : List Nat → Maybe Nat
last l with unsnoc l
... | nothing = nothing
... | just (_ , snd) = just snd

record Cut (l : List Nat) : Set where
  constructor cut
  field
    cuts : List Nat
    mono : monotonic< cuts
    bounds : All (_≤ length l) cuts
    fst : head cuts ≡ just 0
    lst : last cuts ≡ just (length l)

non-empty : ∀ {l} → (c : Cut l) → Cut.cuts c ≢ []
non-empty (cut [] mono bounds () ()) _
non-empty (cut (x₁ ∷ cuts) mono bounds fst lst) ()

mkSegments : (l : List Nat) → Segments l
mkSegments l = {!!}
