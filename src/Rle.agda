-- Implementation and verification of the run length encoding algorithm
module _ where

open import Agda.Builtin.Nat using (Nat; zero; suc)
open import Data.List using (List; []; _∷_; replicate; _++_; foldl)
open import Data.List.Properties using (++-identityʳ; ++-assoc)
open import Data.Maybe using (Maybe; just; nothing)
open import Data.Nat.Properties using (_≟_)
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; sym; trans)
open import Relation.Nullary using (yes; no)

Byte : Set
Byte = Nat

ByteString : Set
ByteString = List Byte

-- Algorithm

rle : Maybe (Nat × Byte) → ByteString → List (Nat × Byte) → List (Nat × Byte)
rle nothing [] ac = ac
rle (just (k , b)) [] ac = (k , b) ∷ ac
rle nothing (x ∷ tail) ac = rle (just (1 , x)) tail ac
rle (just (k , b)) (x ∷ tail) ac with b ≟ x
... | yes p = rle ((just (suc k , b))) tail ac
... | no p = rle (just (1 , x)) tail ((k , b) ∷ ac)

compress-rle : ByteString → List (Nat × Byte)
compress-rle l = rle nothing l []

dec-step : ByteString → (Nat × Byte) → ByteString
dec-step = λ ac y → replicate (proj₁ y) (proj₂ y) ++ ac

decompress-rle : List (Nat × Byte) → ByteString
decompress-rle = foldl dec-step []

-- Proofs

decompress-head : ∀ l k x → decompress-rle ((k , x) ∷ l) ≡ decompress-rle l ++ replicate k x
decompress-head l k x = trans s1 (s2 l [] k x)
  where
  s1 : decompress-rle ((k , x) ∷ l) ≡ foldl dec-step (replicate k x) l
  s1 rewrite ++-identityʳ (replicate k x) = refl
  s2 : ∀ l ac k x → foldl dec-step (ac ++ replicate k x) l ≡ foldl dec-step ac l ++ replicate k x
  s2 [] ac k x = refl
  s2 ((k1 , x1) ∷ l) ac k x rewrite
      ++-identityʳ (replicate k1 x1)
      | sym (s2 l (replicate k1 x1 ++ ac) k x)
      | ++-assoc (replicate k1 x1) ac (replicate k x)
    = refl

lemma-replicate : ∀ bs k (x : Byte) → x ∷ replicate k x ++ bs ≡ replicate k x ++ x ∷ bs
lemma-replicate bs zero x = refl
lemma-replicate bs (suc k) x rewrite lemma-replicate bs k x = refl

decompress-just : ∀ k x bs l → decompress-rle (rle (just (k , x)) bs l) ≡ decompress-rle l ++ (replicate k x ++ bs)
decompress-just k x [] l rewrite
   ++-identityʳ (replicate k x)
   | sym (decompress-head l k x)
   | ++-identityʳ (replicate k x)
   = refl
decompress-just k x (y ∷ bs) ac with x ≟ y
... | yes refl rewrite
  decompress-just (suc k) x bs ac
  | lemma-replicate bs k x
  = refl
... | no _ rewrite
  decompress-just 1 y bs ((k , x) ∷ ac)
  | ++-identityʳ (replicate k x)
  | sym (++-assoc (foldl dec-step [] ac) (replicate k x) (y ∷ bs))
  | sym (decompress-head ac k x)
  | ++-identityʳ (replicate k x)
  = refl

decompress-nothing : ∀ b ac → decompress-rle (rle nothing b ac) ≡ decompress-rle ac ++ b
decompress-nothing [] [] = refl
decompress-nothing [] (x ∷ ac) =  sym (++-identityʳ (decompress-rle (x ∷ ac)))
decompress-nothing (x ∷ b) ac = decompress-just 1 x b ac

-- Main theorem
inverse-rle : ∀ b → decompress-rle (compress-rle b) ≡ b
inverse-rle b = decompress-nothing b []
